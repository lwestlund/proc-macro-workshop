use proc_macro::TokenStream;
use quote::{format_ident, quote};

#[proc_macro_derive(Builder, attributes(builder))]
pub fn derive(input: TokenStream) -> TokenStream {
    let input = syn::parse_macro_input!(input as syn::DeriveInput);

    let tokens = match input.data {
        syn::Data::Struct(syn::DataStruct {
            fields: syn::Fields::Named(fields),
            ..
        }) => {
            let derived_obj_ident = input.ident;
            let builder_ident = format_ident!("{}Builder", derived_obj_ident);

            let attribute_errors = fields.named.iter().filter_map(|f| {
                if let Some((false, err_token_stream)) =
                    get_field_attribute_value("builder", "each", f)
                {
                    Some(quote! {
                        #err_token_stream
                    })
                } else {
                    None
                }
            });
            if let Some(errors) = attribute_errors.reduce(|e, _| e) {
                return errors.into();
            }

            let builder_fields = fields.named.iter().map(|f| {
                let name = &f.ident;
                let ty = &f.ty;
                // Check if there is an outer type Option in order to not wrap
                // Option<T> in another Option.
                let is_option_type = inner_type_of("Option", ty).is_some();
                // Check if there is a builder attribute on the field, in which
                // case we should not wrap it in Option either.
                let has_builder_attrib = get_attribute("builder", f).is_some();

                if is_option_type || has_builder_attrib {
                    quote! { #name: #ty }
                } else {
                    quote! { #name: ::std::option::Option<#ty> }
                }
            });

            // Setters functions for single elements into vector fields and non-vector type fields.
            let setters = fields.named.iter().filter_map(|f| {
                let name = &f.ident;
                let ty = &f.ty;
                if let Some((true, each_value)) = get_field_attribute_value("builder", "each", f) {
                    if let Some(inner_type) = inner_type_of("Vec", ty) {
                        return Some(quote! {
                            fn #each_value(&mut self, #each_value: #inner_type) -> &mut Self {
                                self.#name.push(#each_value);
                                self
                            }
                        });
                    }
                }
                None
            });

            // Setter functions for the builder fields.
            let vec_setters = fields.named.iter().filter_map(|f| {
                let name = &f.ident;
                let outer_type = &f.ty;

                // Skip generating a setter accepting a full vector if there is a
                // #[builder(each = "...")]
                // attribute on the field.
                let attrib_value = get_field_attribute_value("builder", "each", f);
                if let Some((true, ident)) = &attrib_value {
                    if *name.as_ref().unwrap() == ident.to_string() {
                        return None;
                    }
                }

                // Get inner type T of outer type Option<T> or just T if outer type is T.
                let inner_type = inner_type_of("Option", outer_type).unwrap_or(outer_type);
                let assign_value = if attrib_value.is_some() {
                    quote! { #name }
                } else {
                    quote! { ::std::option::Option::Some(#name) }
                };
                Some(quote! {
                    fn #name(&mut self, #name: #inner_type) -> &mut Self {
                        self.#name = #assign_value;
                        self
                    }
                })
            });

            let builder_build_fields = fields.named.iter().map(|f| {
                let name = &f.ident;
                let ty = &f.ty;
                let err = format!(
                    "{}::build(): `{}` never set",
                    builder_ident,
                    name.as_ref().unwrap()
                );

                let is_option_type = inner_type_of("Option", ty).is_some();
                let has_builder_each_attrib =
                    get_field_attribute_value("builder", "each", f).is_some();

                if is_option_type || has_builder_each_attrib {
                    quote! { #name: self.#name.clone() }
                } else {
                    quote! { #name: self.#name.clone().ok_or(#err)? }
                }
            });

            // Default builder fields for when creating an empty builder.
            let builder_default_fields = fields.named.iter().map(|f| {
                let name = &f.ident;

                if get_field_attribute_value("builder", "each", f).is_some() {
                    quote! {
                        #name: ::std::vec::Vec::new()
                    }
                } else {
                    quote! {
                        #name: ::std::option::Option::None
                    }
                }
            });

            // The final output.
            quote! {
                pub struct #builder_ident {
                    #(#builder_fields),*
                }

                impl #builder_ident {
                    pub fn build(
                        &mut self
                    ) -> ::std::result::Result<#derived_obj_ident, ::std::boxed::Box<dyn ::std::error::Error>> {
                        ::std::result::Result::Ok(#derived_obj_ident {
                            #(#builder_build_fields),*
                        })
                    }

                    #(#vec_setters)*
                    #(#setters)*
                }

                impl #derived_obj_ident {
                    pub fn builder() -> #builder_ident {
                        #builder_ident {
                            #(#builder_default_fields),*
                        }
                    }
                }
            }
        }
        _ => syn::Error::new(
            input.ident.span(),
            "cannot create builder for non struct types",
        )
        .to_compile_error(),
    };
    tokens.into()
}

fn inner_type_of<'a>(outer_type: &str, ty: &'a syn::Type) -> Option<&'a syn::Type> {
    if let syn::Type::Path(syn::TypePath {
        qself: None,
        path: syn::Path { segments, .. },
    }) = ty
    {
        match segments.first() {
            Some(syn::PathSegment { ident, arguments }) => {
                if ident == outer_type {
                    if let syn::PathArguments::AngleBracketed(angle_inner_type) = arguments {
                        if angle_inner_type.args.len() == 1 {
                            if let syn::GenericArgument::Type(ty2) = &angle_inner_type.args[0] {
                                return Some(ty2);
                            }
                        }
                    }
                }
            }
            _ => return None,
        }
    }
    None
}

// Get the value of a field attribute `attr_ident`.
//
// #[attr_ident(key = value)]
// field: T,
//
fn get_field_attribute_value<'a>(
    attr_ident: &str,
    key: &str,
    field: &'a syn::Field,
) -> Option<(bool, proc_macro2::TokenStream)> {
    fn make_compile_error<T: quote::ToTokens, U: std::fmt::Display>(
        tokens: T,
        message: U,
    ) -> proc_macro2::TokenStream {
        syn::Error::new_spanned(tokens, message).to_compile_error()
    }

    if let Some(attr) = get_attribute(attr_ident, field) {
        match attr.parse_meta() {
            Ok(meta) => match &meta {
                syn::Meta::List(meta_list) => {
                    if meta_list.nested.len() != 1 {
                        return Some((
                            false,
                            make_compile_error(
                                attr,
                                "multiple attributes on field is not supported",
                            ),
                        ));
                    }

                    match &meta_list.nested[0] {
                        syn::NestedMeta::Meta(syn::Meta::NameValue(name_value)) => {
                            if !name_value.path.is_ident(key) {
                                return Some((
                                    false,
                                    make_compile_error(
                                        meta_list,
                                        format!("expected `{}({} = \"...\")`", attr_ident, key),
                                    ),
                                ));
                            }

                            match &name_value.lit {
                                syn::Lit::Str(lit_str) => {
                                    let value = lit_str.value();
                                    let ident =
                                        quote::format_ident!("{}", value, span = lit_str.span());
                                    use quote::ToTokens;
                                    return Some((true, ident.to_token_stream()));
                                }
                                other => {
                                    return Some((
                                        false,
                                        make_compile_error(other, "is not a literal value"),
                                    ));
                                }
                            }
                        }
                        other => {
                            return Some((
                                false,
                                make_compile_error(
                                    other,
                                    "attribute does not contain a name value pair",
                                ),
                            ));
                        }
                    }
                }
                other => {
                    return Some((false, make_compile_error(other, "unsupported attribute")));
                }
            },
            Err(err) => {
                return Some((false, err.to_compile_error()));
            }
        }
    }
    None
}

// Get the attribute of `attr_ident` from the a struct field `field`.
//
// #[attr_ident(attribute)]
// field: T,
//
fn get_attribute<'a>(attr_ident: &str, field: &'a syn::Field) -> Option<&'a syn::Attribute> {
    for attr in &field.attrs {
        if attr.path.is_ident(attr_ident) {
            return Some(attr);
        }
    }
    None
}

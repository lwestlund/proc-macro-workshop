// Have the macro produce a compiler error for non struct types, e.g. an enum.
//
// The compiler error should say "cannot create builder for non struct types".

use derive_builder::Builder;

#[derive(Builder)]
pub enum NotStructType {}

fn main() {}
